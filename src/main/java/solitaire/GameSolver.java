package solitaire;

import solitaire.gui.SolverPanel;

import java.util.Stack;

import static solitaire.GameUtil.*;

/**
 * Created by Piotr on 2016-10-23.
 */
public class GameSolver extends Thread {
    private GameField gameField;
    private SolverPanel panel;

    private Stack<MoveRecord> moveRegister;

    public GameSolver(SolverPanel panel) {
        gameField = new GameField();
        moveRegister = new Stack<MoveRecord>();
        this.panel = panel;
    }

    @Override
    public void run() {
        solve();
    }

    private void solve() {
        boolean breakLoop;
        int bestResult = 100;

        while(!isGameFinished()) {
            MoveRecord revertedMove = revertLastMove();

            int bi, bj;

            if(revertedMove != null) {
                bi = revertedMove.getPosFromX();
                bj = revertedMove.getPosFromY();
            } else {
                bi = 0;
                bj = 0;
            }

            while (isAnyMovePossible(revertedMove)) {
                breakLoop = false;
                for (int i = bi; i < 7; i++) {
                    if (breakLoop) {
                        break;
                    }
                    for (int j = bj; j < 7; j++) {
                        boolean[] possibleMoves = getPossibleMovesFromPosition(gameField, revertedMove, i, j);
                        if (hasAnyValue(possibleMoves, true)) {
                            performMove(revertedMove, possibleMoves, i, j);
                            breakLoop = true;
                            break;
                        }
                    }
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bi = 0;
                bj = 0;
                revertedMove = null;
                panel.repaint();

                int localResult = countFields(FIELD_FILLED);

                if(localResult < bestResult) {
                    bestResult = localResult;
                }

                System.out.println(bestResult);
            }
        }
    }

    public GameField getGameField() {
        return gameField;
    }

    private MoveRecord revertLastMove() {
        if(moveRegister.size() > 0) {
            MoveRecord lastMove = moveRegister.pop();

            gameField.setField(lastMove.getPosFromX(), lastMove.getPosFromY(), FIELD_FILLED);
            gameField.setField(lastMove.getPosMiddleX(), lastMove.getPosMiddleY(), FIELD_FILLED);
            gameField.setField(lastMove.getPosToX(), lastMove.getPosToY(), FIELD_EMPTY);

            return lastMove;
        }

        return null;
    }

    private void performMove(MoveRecord revertedMove, boolean[] possibleMoves, int posX, int posY) {
        MoveRecord moveRecord = null;

        if(possibleMoves[MOVES_ARRAY_UP]) {
            moveRecord = moveUp(gameField, revertedMove, posX, posY);
        } else if(possibleMoves[MOVES_ARRAY_UP_RIGHT]) {
            moveRecord = moveUpRight(gameField, revertedMove, posX, posY);
        }  else if(possibleMoves[MOVES_ARRAY_RIGHT]) {
            moveRecord = moveRight(gameField, revertedMove, posX, posY);
        } else if(possibleMoves[MOVES_ARRAY_DOWN_RIGHT]) {
            moveRecord = moveDownRight(gameField, revertedMove, posX, posY);
        } else if(possibleMoves[MOVES_ARRAY_DOWN]) {
            moveRecord = moveDown(gameField, revertedMove, posX, posY);
        } else if(possibleMoves[MOVES_ARRAY_LEFT_DOWN]) {
            moveRecord = moveLeftDown(gameField, revertedMove, posX, posY);
        } else if(possibleMoves[MOVES_ARRAY_LEFT]) {
            moveRecord = moveLeft(gameField, revertedMove, posX, posY);
        } else if(possibleMoves[MOVES_ARRAY_UP_LEFT]) {
            moveRecord = moveUpLeft(gameField, revertedMove, posX, posY);
        }

        registerMove(moveRecord);
    }

    private void registerMove(MoveRecord moveRecord) {
        if(moveRecord != null) {
            moveRegister.push(moveRecord);
        }
    }

    private boolean isGameFinished() {
        return countFields(FIELD_FILLED) == 1;
    }

    private int countFields(int field) {
        int counter = 0;
        for(int i=0; i<7; i++) {
            for (int j = 0; j < 7; j++) {
                if(gameField.getField(i, j) == field) {
                    counter ++;
                }
            }
        }

        return counter;
    }

    private boolean isAnyMovePossible(MoveRecord moveRecord) {
        if(moveRecord == null) {
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < 7; j++) {
                    boolean[] possibleMoves = getPossibleMovesFromPosition(gameField, moveRecord, i, j);
                    if (hasAnyValue(possibleMoves, true)) {
                        return true;
                    }
                }
            }
        } else {
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < 7; j++) {
                    boolean[] possibleMoves = getPossibleMovesFromPosition(gameField, moveRecord, i, j);
                    if (hasAnyValue(possibleMoves, true) && hasAnyValue(moveRecord.getWays(), false)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean hasAnyValue(boolean[] possibleMoves, boolean value) {
        for(int i=0; i<possibleMoves.length; i++) {
            if(possibleMoves[i] == value) {
                return true;
            }
        }

        return false;
    }
}

package solitaire;

/**
 * Created by Piotr on 2016-10-23.
 */
public class GameUtil {
    public static final int FIELD_NOT_GAME_FIELD    = 0;
    public static final int FIELD_FILLED            = 1;
    public static final int FIELD_EMPTY             = 2;
    
    public static final int MOVES_ARRAY_UP          = 0;
    public static final int MOVES_ARRAY_UP_RIGHT    = 1;
    public static final int MOVES_ARRAY_RIGHT       = 2;
    public static final int MOVES_ARRAY_DOWN_RIGHT  = 3;
    public static final int MOVES_ARRAY_DOWN        = 4;
    public static final int MOVES_ARRAY_LEFT_DOWN   = 5;
    public static final int MOVES_ARRAY_LEFT        = 6;
    public static final int MOVES_ARRAY_UP_LEFT     = 7;
    
    public static void printGameField(GameField gameField){
        char charToPrint;
        
        for(int i=0; i<7; i++) {
            for(int j=0; j<7; j++) {
                int value = gameField.getField(i, j);
                
                if(value == FIELD_NOT_GAME_FIELD) {
                    charToPrint = ' ';
                } else if(value == FIELD_FILLED) {
                    charToPrint = '#';
                } else {
                    charToPrint = 'O';
                }

                System.out.print(charToPrint);
            }
            System.out.println();
        }
    }

    public static boolean isInRange(int value, int a, int b) {
        return value >=a && value <=b;
    }

    public static MoveRecord moveUp(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x - 1, y, FIELD_EMPTY);
        gameField.setField(x - 2, y, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x-1)
                .withPosMiddleY(y)
                .withPosToX(x-2)
                .withPosToY(y)
                .addWay(MOVES_ARRAY_UP)
                .toMoveRecord();

    }

    public static boolean isMoveUpPossible(GameField gameField, int posX, int posY) {
        int newPosX = posX - 2;
        return isInRange(newPosX, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(newPosX, posY) == FIELD_EMPTY
                && gameField.getField(posX - 1, posY) == FIELD_FILLED;

    }

    public static MoveRecord moveUpRight(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x - 1, y + 1, FIELD_EMPTY);
        gameField.setField(x - 2, y + 2, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x-1)
                .withPosMiddleY(y+1)
                .withPosToX(x-2)
                .withPosToY(y+2)
                .addWay(MOVES_ARRAY_UP_RIGHT)
                .toMoveRecord();
    }

    public static boolean isMoveUpRightPossible(GameField gameField, int posX, int posY) {
        int newPosX = posX - 2;
        int newPosY = posY + 2;
        return isInRange(newPosX, 0, 6) && isInRange(newPosY, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(newPosX, newPosY) == FIELD_EMPTY
                && gameField.getField(posX - 1, posY + 1) == FIELD_FILLED;
    }

    public static MoveRecord moveRight(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x, y + 1, FIELD_EMPTY);
        gameField.setField(x, y + 2, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x)
                .withPosMiddleY(y+1)
                .withPosToX(x)
                .withPosToY(y+2)
                .addWay(MOVES_ARRAY_RIGHT)
                .toMoveRecord();
    }

    public static boolean isMoveRight(GameField gameField, int posX, int posY) {
        int newPosY = posY + 2;
        return isInRange(newPosY, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(posX, newPosY) == FIELD_EMPTY
                && gameField.getField(posX, posY + 1) == FIELD_FILLED;
    }

    public static MoveRecord moveDownRight(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x + 1, y + 1, FIELD_EMPTY);
        gameField.setField(x + 2, y + 2, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x+1)
                .withPosMiddleY(y+1)
                .withPosToX(x+2)
                .withPosToY(y+2)
                .addWay(MOVES_ARRAY_DOWN_RIGHT)
                .toMoveRecord();
    }

    public static boolean isMoveDownRightPossible(GameField gameField, int posX, int posY) {
        int newPosX = posX + 2;
        int newPosY = posY + 2;
        return isInRange(newPosX, 0, 6) && isInRange(newPosY, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(newPosX, newPosY) == FIELD_EMPTY
                && gameField.getField(posX + 1, posY + 1) == FIELD_FILLED;
    }

    public static MoveRecord moveDown(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x + 1, y, FIELD_EMPTY);
        gameField.setField(x + 2, y, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x+1)
                .withPosMiddleY(y)
                .withPosToX(x+2)
                .withPosToY(y)
                .addWay(MOVES_ARRAY_DOWN)
                .toMoveRecord();
    }

    public static boolean isMoveDownPossible(GameField gameField, int posX, int posY) {
        int newPosX = posX + 2;
        return isInRange(newPosX, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(newPosX, posY) == FIELD_EMPTY
                && gameField.getField(posX + 1, posY) == FIELD_FILLED;
    }

    public static MoveRecord moveLeftDown(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x + 1, y - 1, FIELD_EMPTY);
        gameField.setField(x + 2, y - 2, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x+1)
                .withPosMiddleY(y-1)
                .withPosToX(x+2)
                .withPosToY(y-2)
                .addWay(MOVES_ARRAY_LEFT_DOWN)
                .toMoveRecord();
    }

    public static boolean isMoveLeftDownPossible(GameField gameField, int posX, int posY) {
        int newPosX = posX + 2;
        int newPosY = posY - 2;
        return isInRange(newPosX, 0, 6) && isInRange(newPosY, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(newPosX, newPosY) == FIELD_EMPTY
                && gameField.getField(posX + 1, posY - 1) == FIELD_FILLED;
    }

    public static MoveRecord moveLeft(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x, y - 1, FIELD_EMPTY);
        gameField.setField(x, y - 2, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x)
                .withPosMiddleY(y-1)
                .withPosToX(x)
                .withPosToY(y-2)
                .addWay(MOVES_ARRAY_LEFT)
                .toMoveRecord();
    }

    public static boolean isMoveLeftPossible(GameField gameField, int posX, int posY) {
        int newPosY = posY - 2;
        return isInRange(newPosY, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(posX, newPosY) == FIELD_EMPTY
                && gameField.getField(posX, posY - 1) == FIELD_FILLED;
    }

    public static MoveRecord moveUpLeft(GameField gameField, MoveRecord revertedMove, int x, int y) {
        gameField.setField(x, y, FIELD_EMPTY);
        gameField.setField(x - 1, y - 1, FIELD_EMPTY);
        gameField.setField(x - 2, y - 2, FIELD_FILLED);

        return new MoveRecord.MoveRecordBuilder(revertedMove)
                .withPosFromX(x)
                .withPosFromY(y)
                .withPosMiddleX(x-1)
                .withPosMiddleY(y-1)
                .withPosToX(x-2)
                .withPosToY(y-2)
                .addWay(MOVES_ARRAY_UP_LEFT)
                .toMoveRecord();
    }

    public static boolean isMoveUpLeftPossible(GameField gameField, int posX, int posY) {
        int newPosX = posX - 2;
        int newPosY = posY - 2;
        return isInRange(newPosX, 0, 6) && isInRange(newPosY, 0, 6)
                && gameField.getField(posX, posY) == FIELD_FILLED
                && gameField.getField(newPosX, newPosY) == FIELD_EMPTY
                && gameField.getField(posX - 1, posY - 1) == FIELD_FILLED;
    }

    public static boolean checkRevertedMovePossible(MoveRecord revertedMove, int move) {
        if(revertedMove != null) {
            return revertedMove.getWays()[move] == false;
        }

        return true;
    }

    public static boolean[] getPossibleMovesFromPosition(GameField gameField, MoveRecord revertedMove, int posX,  int posY) {
        boolean[] movesArray = new boolean[]{false, false, false, false, false, false, false, false};

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_UP)
                && GameUtil.isMoveUpPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_UP] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_UP_RIGHT)
                && GameUtil.isMoveUpRightPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_UP_RIGHT] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_RIGHT)
                && GameUtil.isMoveRight(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_RIGHT] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_DOWN_RIGHT)
                && GameUtil.isMoveDownRightPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_DOWN_RIGHT] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_DOWN)
                && GameUtil.isMoveDownPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_DOWN] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_LEFT_DOWN)
                && GameUtil.isMoveLeftDownPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_LEFT_DOWN] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_LEFT)
                && GameUtil.isMoveLeftPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_LEFT] = true;
        }

        if(checkRevertedMovePossible(revertedMove, MOVES_ARRAY_UP_LEFT)
                && GameUtil.isMoveUpLeftPossible(gameField, posX, posY)) {
            movesArray[MOVES_ARRAY_UP_LEFT] = true;
        }

        return movesArray;
    }
}

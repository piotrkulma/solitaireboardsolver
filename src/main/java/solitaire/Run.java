package solitaire;

import solitaire.gui.SolverFrame;

import java.awt.*;

/**
 * Created by Piotr on 2016-10-23.
 */
public class Run {
    public static void main(String... args) {
        //GameSolver gameSolver = new GameSolver();
        //gameSolver.solve();

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SolverFrame();
            }
        });
    }
}

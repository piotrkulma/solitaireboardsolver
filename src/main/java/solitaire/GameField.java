package solitaire;

/**
 * Created by Piotr on 2016-10-23.
 */
public class GameField {
    private int[][] GAME_FIELD =
            {
                    {0, 0, 1, 1, 1, 0, 0},
                    {0, 0, 1, 1, 1, 0, 0},
                    {1, 1, 1, 1, 1, 1, 1},
                    {1, 1, 1, 2, 1, 1, 1},
                    {1, 1, 1, 1, 1, 1, 1},
                    {0, 0, 1, 1, 1, 0, 0},
                    {0, 0, 1, 1, 1, 0, 0}
            };

    public GameField() {
    }

    public int getField(int x, int y) {
        return GAME_FIELD[x][y];
    }

    public void setField(int x, int y, int value) {
        GAME_FIELD[x][y] = value;
    }
}

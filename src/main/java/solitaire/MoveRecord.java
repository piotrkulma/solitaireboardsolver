package solitaire;

/**
 * Created by Piotr on 2016-10-23.
 */
public final class MoveRecord {
    private int posFromX;
    private int posFromY;

    private int posMiddleX;
    private int posMiddleY;

    private int posToX;
    private int posToY;
    private boolean[] ways;

    public int getPosFromX() {
        return posFromX;
    }

    public int getPosFromY() {
        return posFromY;
    }

    public int getPosMiddleX() {
        return posMiddleX;
    }

    public int getPosMiddleY() {
        return posMiddleY;
    }

    public int getPosToX() {
        return posToX;
    }

    public int getPosToY() {
        return posToY;
    }

    public boolean[] getWays() {
        return ways;
    }

    private MoveRecord(MoveRecordBuilder builder) {
        this.posFromX = builder.posFromX;
        this.posFromY = builder.posFromY;
        this.posMiddleX = builder.posMiddleX;
        this.posMiddleY = builder.posMiddleY;
        this.posToX = builder.posToX;
        this.posToY = builder.posToY;
        this.ways = builder.ways;
    }

    public static final class MoveRecordBuilder {
        private int posFromX;
        private int posFromY;

        private int posMiddleX;
        private int posMiddleY;

        private int posToX;
        private int posToY;

        private boolean[] ways;

        public MoveRecordBuilder(MoveRecord moveRecord) {
            if(moveRecord != null) {
                this.ways = moveRecord.getWays();
            } else {
                this.ways = new boolean[] {false, false, false, false, false, false, false, false};
            }
        }

        public MoveRecordBuilder withPosFromX(int posFromX) {
            this.posFromX = posFromX;
            return this;
        }

        public MoveRecordBuilder withPosFromY(int posFromY) {
            this.posFromY = posFromY;
            return this;
        }

        public MoveRecordBuilder withPosMiddleX(int posMiddleX) {
            this.posMiddleX = posMiddleX;
            return this;
        }

        public MoveRecordBuilder withPosMiddleY(int posMiddleY) {
            this.posMiddleY = posMiddleY;
            return this;
        }

        public MoveRecordBuilder withPosToX(int posToX) {
            this.posToX = posToX;
            return this;
        }

        public MoveRecordBuilder withPosToY(int posToY) {
            this.posToY = posToY;
            return this;
        }

        public MoveRecordBuilder addWay(int wayIndex) {
            this.ways[wayIndex] = true;
            return this;
        }

        public MoveRecordBuilder copyWays(boolean[] ways) {
            this.ways = ways;
            return this;
        }

        public MoveRecord toMoveRecord() {
            return new MoveRecord(this);
        }
    }
}

package solitaire.gui;

import solitaire.GameSolver;
import solitaire.GameUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by Piotr on 2016-10-23.
 */
public class SolverPanel extends JPanel {
    private GameSolver solver;
    public SolverPanel() {
        solver = new GameSolver(this);
        solver.start();
    }

    private void paintFields(Graphics2D g2d) {
        for (int i=0; i<7; i++) {
            for(int j=0; j<7; j++) {
                int field = solver.getGameField().getField(i, j);

                if(field == GameUtil.FIELD_FILLED) {
                    g2d.setColor(Color.BLACK);
                }else if(field == GameUtil.FIELD_EMPTY) {
                    g2d.setColor(Color.BLUE);
                }else if(field == GameUtil.FIELD_NOT_GAME_FIELD) {
                    g2d.setColor(Color.WHITE);
                }

                g2d.fillRect(i * 20, j * 20, 20, 20);

            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        paintFields(g2d);
    }
}

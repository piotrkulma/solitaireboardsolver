package solitaire.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Piotr on 2016-10-23.
 */
public class SolverFrame extends JFrame {
    public SolverFrame() {
        super();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 500);

        JPanel panel = new SolverPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        setVisible(true);
    }
}
